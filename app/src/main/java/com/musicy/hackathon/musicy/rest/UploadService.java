package com.musicy.hackathon.musicy.rest;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by Mart on 11/26/2016.
 */

public abstract interface UploadService {

    @Multipart
    @POST("/upload")
    public abstract void upload(@Part("file") TypedFile paramTypedFile, Callback<Response> paramCallback);

    @Multipart
    @POST("/upload")
    public abstract Response uploadSync(@Part("file") TypedFile paramTypedFile);
}
