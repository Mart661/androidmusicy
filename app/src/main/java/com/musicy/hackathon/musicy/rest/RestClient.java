package com.musicy.hackathon.musicy.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Mart on 11/26/2016.
 */

public class RestClient {
    private UploadService  uploadService;
    private String URL ="http://10.0.2.2:49245/api/";

    public RestClient(){
        Gson localGson = new GsonBuilder().create();

        this.uploadService = ((UploadService)new RestAdapter.Builder()
                .setEndpoint(URL)
                .setConverter(new GsonConverter(localGson))
                .setRequestInterceptor(new RequestInterceptor()
                {
                    public void intercept(RequestInterceptor.RequestFacade requestFacade)
                    {   //By adding header to the request will allow us to debug into .Net code in server
                        if (URL.contains("10.0.2.2")) {
                            requestFacade.addHeader("Host", "localhost");
                        }
                    }
                })
                .build().create(UploadService.class));

    }



    public UploadService getService()
    {
        return this.uploadService;
    }


}

