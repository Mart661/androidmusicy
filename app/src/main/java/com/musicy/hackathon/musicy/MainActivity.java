package com.musicy.hackathon.musicy;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.musicy.hackathon.musicy.rest.CancelableCallback;
import com.squareup.picasso.Picasso;
import com.musicy.hackathon.musicy.rest.RestClient;
import com.musicy.hackathon.musicy.rest.UploadService;
import com.musicy.hackathon.musicy.rest.UploadSync;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;


public class MainActivity extends AppCompatActivity {

    private String imagePath="";
    private File savedFileDestination;
    private static final int PICK_CAMERA_IMAGE = 1;

    private Button btnPhoto, btnUpload, btnUploadZip;
    ImageView imgPhoto;
    private RestClient restClient;
    ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        restClient = new RestClient();

        imgPhoto = (ImageView) findViewById(R.id.imgPhoto);

        //Remove this section if you don't want camera code (Start)
        btnPhoto = (Button) findViewById(R.id.btnPhoto);
        btnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
                String name = dateFormat.format(new Date());
                savedFileDestination = new File(imagePath, name + ".jpg");

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(savedFileDestination));
                startActivityForResult(intent, PICK_CAMERA_IMAGE);
                //FileHelper.zip(imagePath, zipPath, "a.zip");

            }
        });
        //Remove this section if you don't want camera code (End)

        btnUploadZip = (Button) findViewById(R.id.btnUploadZip);
        btnUploadZip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UploadSync(MainActivity.this).execute(imagePath);
            }
        });

        //location where photo saved
        imagePath =  Environment.getExternalStorageDirectory().toString() + "/instinctcoder/uploadFiles/";
        new File(imagePath).mkdir();


        btnUpload= (Button) findViewById(R.id.btnUpload);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
            }
        });

    }

    private void initiateProgressDialog(){
        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Uploading files...");
        mProgress.setCancelable(true);

        //setButton is depreciated, it's tell us is not good to cancel when something is running at the back. :). Think about it.
//When cancel button clicked, it will ignore the response from server
        //You could see this from video
        mProgress.setButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CancelableCallback.cancelAll();
                return;
            }
        });
        mProgress.show();
    }


    private void uploadImage(){
        if (savedFileDestination==null) {
            Toast.makeText(this,"Please take photo first", Toast.LENGTH_LONG).show();
            return;
        }

        TypedFile typedFile = new TypedFile("multipart/form-data", savedFileDestination);
        initiateProgressDialog();

        restClient.getService().upload(typedFile, new CancelableCallback<Response>() {
            @Override
            public void onSuccess(Response response, Response response2) {
                mProgress.dismiss();
                Picasso.with(MainActivity.this)
                        .load(savedFileDestination)
                        .into(imgPhoto);


                Toast.makeText(MainActivity.this,"Upload successfully",Toast.LENGTH_LONG).show();
                Log.e("Upload", "success");
            }

            @Override
            public void onFailure(RetrofitError error) {
                mProgress.dismiss();
                Toast.makeText(MainActivity.this,"Upload failed",Toast.LENGTH_LONG).show();
                Log.e("Upload", error.getMessage().toString());
            }
        });


    }

    @Override
    public void onBackPressed(){
        if (CancelableCallback.runningProcess()==false) finish();
    }

    //Remove this section if you don't want camera code (Start)
    //Got new image taken from camera intent
    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data)
    {
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        //For the purpose of when the device orientation change
        savedInstanceState.putString("destination", savedFileDestination.getName());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //For the purpose of when the device orientation change

        String sPath = savedInstanceState.getString("destination");
        savedFileDestination = new File(sPath);
    }
    //Remove this section if you don't want camera code (End)
}